const INITIAL_STATE = {
    activeLesson: {},
    activeModule: {},
    modules: [
        {
            id: 1,
            title: 'Iniciando React',
            lessons: [
                {
                    id: 2,
                    title: 'Terceira Aula'
                },
                {
                    id: 3,
                    title: 'Quarta Aula'
                }
            ]
        },
        {
            id: 2,
            title: 'Iniciando Outra aula',
            lessons: [
                {
                    id: 4,
                    title: 'Terceira Aula'
                },
                {
                    id: 6,
                    title: 'Quarta Aula'
                }
            ]
        }
    ]
};

export default function course(state = INITIAL_STATE, action) {
    console.log(action);
    
    if (action.type === "TOGGLE_LESSON") {
        return {
            ...state,
            activeLesson: action.lesson,
            activeModule: action.module
        };
    }

    return state;
}